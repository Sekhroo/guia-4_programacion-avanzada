package objetos;

import java.util.Scanner;

public class controlador {

	public controlador(){
		Scanner sc=new Scanner(System.in);
		
		// Crear automovil
		automovil atm = new automovil();
		
		// Crear partes del automovil
		motor motor = new motor();
		velocimetro veloc = new velocimetro();
		estanque estanque = new estanque();
		for (int i=0; i<4;i++) {
			atm.setrueda(new ruedas());
		}
		
		// Componer el automovil
		atm.setmotor(motor);
		atm.setvelocimetro(veloc);
		atm.setestanque(estanque);
		
		do {
			System.out.println("---------------------------------------------------------------------------");
			System.out.println("================ Menu =================");
			System.out.println("Encender el automovil -----------> [1]");
			System.out.println("Movimiento del automovil --------> [2]");
			System.out.println("Apagar el automovil -------------> [3]");
			System.out.println("Estado del automovil ------------> [4]");
			System.out.println("=======================================");
			System.out.println("Introduzca comando: ");
			int directriz = sc.nextInt();
			if (directriz == 1) {
				if (atm.getencendido()) {
					System.out.println("El auto ya esta encendido");
					System.out.println("---------------------------------------------------------------------------");
				}else {
					atm.interruptor_automovil(true);
					atm.getestanque().setvolumen(atm.getestanque().getvolumen()-atm.getestanque().getvolumen_max()/100);
					System.out.println("Automovil encendido");
					System.out.println("---------------------------------------------------------------------------");
				}
			}else if (directriz == 2) {
				if (atm.getencendido()) {
					atm.getvelocimetro().setvelocidad(120); 
					atm.movimiento();
				}else {
					System.out.println("El automovil esta apagado");
					System.out.println("---------------------------------------------------------------------------");
				}
			}else if (directriz == 3) {
				atm.interruptor_automovil(false);
				atm.getvelocimetro().setvelocidad(0);
				System.out.println("Automovil apagado");
				System.out.println("---------------------------------------------------------------------------");
			}else if (directriz == 4){
				atm.reporte_automovil();
			}else{
				System.out.println("---------------------------------------------------------------------------");
				System.out.println("introduzca un comando valido");
				System.out.println("---------------------------------------------------------------------------");
			}
			System.out.println("\n");
		}while(atm.getestanque().getvolumen() >= 0.0);
		System.out.println("---------------------------------------------------------------------------");
		System.out.println("Estanque vacio, el automovil quedo en pana...");
		System.out.println("---------------------------------------------------------------------------");
	}
}
