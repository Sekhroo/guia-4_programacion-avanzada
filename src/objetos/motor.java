package objetos;
import java.util.Random;

public class motor {

	private String cilindrada;

	motor(String cilindrada){
		
		this.cilindrada = cilindrada;
	}
	
	motor(){
		// Random para determinar cilindrada
		String cilindrada[] = new String[]{"1,2", "1,6"};
		this.cilindrada = cilindrada[new Random().nextInt(cilindrada.length)];
	}
	
	
	// ------------------------------------------------------------
	// Getters
	// ------------------------------------------------------------
	public String getCilindrada() {
		return cilindrada;
	}
	// ------------------------------------------------------------
}
