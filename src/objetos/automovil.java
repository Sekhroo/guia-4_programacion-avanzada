package objetos;

import java.util.ArrayList;
import java.util.Random;

public class automovil {

	private motor motor;
	private velocimetro velocimetro;
	private ArrayList<ruedas> rueda;
	private estanque estanque;
	private Boolean encendido ;
	
	automovil(){
		this.rueda = new ArrayList<ruedas>();
		this.encendido = false;
	}
	
	
	public void interruptor_automovil(boolean interruptor) {
		this.encendido = interruptor;
	}
	
	public void movimiento() {
		//tiempo
		int tiempo[] = new int[]{1,2,3,4,5,6,7,8,9,10};
		double duracion = tiempo[new Random().nextInt(tiempo.length)];
		double distancia = velocimetro.getvelocidad() * duracion/60;
		if (motor.getCilindrada() == "1,2") {
			estanque.setvolumen(estanque.getvolumen()-distancia/20);
		}else if (motor.getCilindrada() == "1,6") {
			estanque.setvolumen(estanque.getvolumen()-distancia/14);
		}
		System.out.println("El auto se mueve "+duracion+" segundos a 120 Km/min");
		System.out.println("---------------------------------------------------------------------------");
		for (ruedas r : this.rueda) {
			int deterioro = (int) (Math.random()*10+1);
			r.setintegridadr(r.getintegridadr()-deterioro);
			if (r.getintegridadr() <= 10) {
				this.setencendido(false);
				System.out.println("Una de las ruedas se desgasto demaciado, se apago el automovil y se cambio la rueda");
				System.out.println("---------------------------------------------------------------------------");
				r.setintegridadr(100);
			}
		}
	}
	
	public void chequeo_ruedas() {
		for (ruedas r : this.rueda) {
			System.out.println("> "+r.getintegridadr());
		}
	}
	
	public void reporte_automovil() {
		String interruptor;
		if (this.encendido) {
			interruptor = "Encendido";
		}else {
			interruptor = "Apagado";
		}
		System.out.println("");
		System.out.println("El automovil se encuentra: "+ interruptor);
		System.out.println("El automovil tiene un motor de " + this.motor.getCilindrada());
		System.out.println("Cantidad de litros en el estanque: " + this.estanque.getvolumen());
		System.out.println("El estado actual de las ruedas es: ");
		chequeo_ruedas();
		System.out.println("La velocidad actual del automovil es " + this.velocimetro.getvelocidad());
		System.out.println("---------------------------------------------------------------------------");
	}
	
	
	// ------------------------------------------------------------
	// Getters y Setters
	// ------------------------------------------------------------
	public motor getmotor() {
		return motor;
	}
	public void setmotor(motor motor) {
		this.motor = motor;
	}
	// ------------------------------------------------------------
	public velocimetro getvelocimetro() {
		return velocimetro;
	}
	public void setvelocimetro(velocimetro velocimetro) {
		this.velocimetro = velocimetro;
	}
	// ------------------------------------------------------------
	public estanque getestanque() {
		return estanque;
	}
	public void setestanque(estanque estanque) {
		this.estanque = estanque;
	}
	// ------------------------------------------------------------
	public Boolean getencendido() {
		return encendido;
	}
	public void setencendido(Boolean encendido) {
		this.encendido = encendido;
	}
	// ------------------------------------------------------------
	public ArrayList<ruedas> getrueda() {
		return rueda;
	}
	public void setrueda(ruedas rueda) {
		this.rueda.add(rueda);
	}
	// ------------------------------------------------------------
}
